#ifndef ACQUISITION_HANDLER_DEFINITION_H
#define ACQUISITION_HANDLER_DEFINITION_H



namespace ungava
{
namespace cartridge
{

enum onOffState
{
	OFF = 0,
	ON
};

enum automaticDcOffset
{
	DISABLED_OFFSET                 = 0,     // default value
	COMPUTED_AT_COLLECTION_START,
	COMPUTED_PER_FOCAL_LAW,
	COMPUTED_EVERY_SCAN
};

enum triggerMode
{
	SOFTWARE_MODE = 0,       // default value, signel shot
	INTERNAL_MODE,           // Free PRF
	ENCODER_MODE,
	TRIGGER_MODE_RESERVED
};

enum gateNumber
{
	GATE_1 = 0,
	GATE_2,
	GATE_3,
	GATE_4
};

enum gateControlFollow
{
	FROM_T0         = 0,
	FOLLOW_GATE_1,
	FOLLOW_GATE_2,
	FOLLOW_GATE_3
};

enum gateControlType
{
	MAXIMUM_PEAK    = 0,    // default value
	POSITIVE_EDGE   = 2,
	NEGATIVE_EDGE   = 3,
	FIRST_PEAK      = 4,
	SECOND_PEAK     = 6
};

enum encoderSlot
{
	ENCODER_SLOT_0 = 0,
	ENCODER_SLOT_1,
	ENCODER_SLOT_2,
	ENCODER_SLOT_3,
	ENCODER_SLOT_4
};

enum encoderName
{
	ENCODER_1 = 0,
	ENCODER_2
};

enum encoderType
{
	QUADRATUR           = 0,
	STEP_PLUS_DIRECTION
};

enum  rectificationType
{
	BIPOLAR             = 0,
	RAW_SIGNAL_BYPASS,       // default value
	POSITIVE_UNIPOLAR,
	NEGATIVE_UNIPOLAR
};

enum zbtSelfTestBit
{
	ZBT_1_PASS_BIT      = 0x00000001,
	ZBT_DONE_BIT        = 0x00000002,
	ZBT_PROGRESS_BIT    = 0x00000004,
	ZBT_2_PASS_BIT      = 0x00000008,
	ZBT_BIT_INVALID     = 0xFFFFFFFF
};

enum log2AverageValue
{
	NO_AVERAGING                = 0,    // default value
	AVERAGING_OF_2_FOCAL_LAW,
	AVERAGING_OF_4_FOCAL_LAW,
	AVERAGING_OF_8_FOCAL_LAW,
	AVERAGING_OF_16_FOCAL_LAW,
	AVERAGING_OF_32_FOCAL_LAW,
	AVERAGING_OF_64_FOCAL_LAW
};

enum fftSize
{
	FFT_64      = 0x6,
	FFT_128     = 0x7,
	FFT_256     = 0x8,
	FFT_512     = 0x9,
	FFT_1024    = 0xA,  // default value
	FFT_2048    = 0xB,
	FFT_4096    = 0xC,
	FFT_8192    = 0xD
};

enum commandWord
{
	WORD_0 = 0,
	WORD_1,
	WORD_2,
	WORD_3,
	WORD_4
};

enum interfaceTriggerMode
{
	START_AT_INTERFACE      = 0,
	START_BEFORE_INTERFACE,
	START_AFTER_INTERFACE
};

enum commModuleType
{
    COMM_NONE             = 0,
    COMM_WIFI_BLUETOOTH,
    COMM_RESERVED,
    COMM_EHTERNET
};


enum encoderReportingType
{
    ENCODER_REPORTING_TICK = 0,
    ENCODER_REPORTING_GRID
};


}  // ungava
}  // cartridge

#endif // ACQUISITION_HANDLER_DEFINITION_H
