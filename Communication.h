#ifndef COMMUNICATION_H
#define COMMUNICATION_H

namespace ungava
{
enum CommunicationType
{
    PCIE_BUS = 0,
    USB_PORT,
    ETHERNET_PORT,
    BLUETOOTH_PORT
};

enum CommunicationCode
{
    COMM_OK = 0,
    COMM_ERROR = -5000,
    COMM_SOCKET_ERROR = -5001,
    COMM_WRONG_ANSWER_TYPE = -5002,
    COMM_NOT_IMPLEMENTED = -5100,
};

enum IpConfigType
{
    ACTIVE_IP = 0,
    FAIL_SAFE_IP
};


}  // ungava
#endif
