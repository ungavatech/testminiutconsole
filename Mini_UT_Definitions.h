#ifndef MINI_UT_DEFINITION_H
#define MINI_UT_DEFINITION_H


#ifdef _WIN32
typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned long long uint64_t;
#else
typedef unsigned short     uint16_t;
typedef short              int16_t;
typedef int                uint32_t;
#endif



namespace ungava
{
namespace cartridge
{
namespace mini_ut
{

enum miniutHandlerErrorCode
{
    MINI_UT_NO_ERROR = 0,
    MINI_UT_CHANNEL_NUMBER_OUT_OF_RANGE = -6001,
    MINI_UT_GREATHER_THAN_DAQ_COUNT = -6002,
    MINI_UT_BAD_FOLLOW = -6003,
    MINI_UT_MEMORY_ALLOCATION_ERROR = -6004,
    MINI_UT_ZBT_SELF_TEST_BIT_INVALID = -6005,
    MINI_UT_BAD_RX_TX_COMBINATION = -6006,
    MINI_UT_ERROR_USB_INITIALIZATION = -6007,
    MINI_UT_HIGH_VOLTAGE_OUT_OF_RANGE = -6008,
    MINI_UT_PULSE_WIDTH_OUT_OF_RANGE = -6009,
    MINI_UT_RX_GAIN_OUT_OF_RANGE = -6010,
    MINI_UT_RX_GAIN_TIME_OUT_OF_RANGE = -6011,
    MINI_UT_RX_GAIN_SLOPE_OUT_OF_RANGE = -6012,
    MINI_UT_RX_GAIN_TCG_MAX_OUT_OF_RANGE = -6013,
    MINI_UT_DAQ_DELAY_OUT_OF_RANGE = -6014,
    MINI_UT_DAQ_RANGE_OUT_OF_RANGE = -6015,
    MINI_UT_COMPRESSION_FACTOR_OUT_OF_RANGE = -6016,
    MINI_UT_INVALID_TRIGGER_MODE = -6017,
    MINI_UT_TRIGGER_INTERNAL_FREQUENCY_OUT_OF_RANGE = -6018,
    MINI_UT_COLLECT_NUMBER_TRIGGER_OUT_OF_RANGE = -6019,
    MINI_UT_HEADER_FOOTER_ASCAN_OUT_OF_RANGE = -6020,
    MINI_UT_GATE_START_OUT_OF_RANGE = -6021,
    MINI_UT_GATE_END_OUT_OF_RANGE = -6022,
    MINI_UT_GATE_THRESHOLD_OUT_OF_RANGE = -6023,
    MINI_UT_GATE1_INTERFACE_TRIGGER_DELAY_OF_RANGE = -6024,
    MINI_UT_ENCODER_TRIG_INCREMENT_OF_RANGE = -6025,
    MINI_UT_BAD_GAIN_SLOPE = -6026,
    MINI_UT_BAD_GAIN_TIME = -6027,
    MINI_UT_PRODUCTION_MODE_ONLY = -6028,
    MINI_UT_ERROR_ETHERNET_INITIALIZATION = -6029,
    MINI_UT_ERROR_PCI_E_INITIALIZATION = -6030,
    MINI_UT_ERROR_BLUETOOTH_INITIALIZATION = -6031,
    MINI_UT_ERROR_READING_VALUE = -6032,
    MINI_UT_ERROR_ILLEGAL_RX_TX_PAIR = -6033,
    MINI_UT_WRITE_REGISTER_ERROR = -6034,
    MINI_UT_READ_REGISTER_ERROR = -6035,
    MINI_UT_READ_ASCAN_ERROR = -6036,
    MINI_UT_UPDATE_FIRMWARE_ERROR = -6037,
    MINI_UT_INVALID_COMMAND = -6038,
    MINI_UT_UPDATE_WIRELESS_FIRMWARE_ERROR = -6039,
};

enum samplingFrequency
{
    HUNDRED_MHZ = 0,
    FIFTY_MHZ,
    TWENTY_FIVE_MHZ
};

enum adcPrecision
{
    TWELVE_BITS = 0,
    EIGHT_BITS
};

enum muxChainBit
{
    BIT0 = 0x01,
    BIT1 = 0x02,
    BIT2 = 0x04,
    BIT3 = 0x08,
    BIT4 = 0x10,
    BIT5 = 0x20,
    BIT6 = 0x40,
    BIT7 = 0x80
};

enum bandpassFilterFreq
{
    BANDPASS_NONE = 0,
    BANDPASS_15MHZ,
    BANDPASS_10MHZ,
    BANDPASS_5MHZ,
    BANDPASS_2_5MHZ,
    BANDPASS_1MHZ
};

enum firFilterFreq
{
    FIR_FILTER_NONE = 0,
    FIR_FILTER_BANDPASS_1MHZ,
    FIR_FILTER_BANDPASS_2_5MHZ,
    FIR_FILTER_BANDPASS_5MHZ,
    FIR_FILTER_BANDPASS_10MHZ,
    FIR_FILTER_BANDPASS_15MHZ
};

enum miniutPhysicalEncoderType
{
    MINI_UT_NO_ENCODER = 0,  // default value
    MINI_UT_MECHANICAL_ENCODER
};

enum miniutFirmwareUpdateType
{
    MINI_UT_FAILSAFE = 0,
    MINI_UT_ACTIVE = 1
};


typedef struct
{
    uint16_t    scanNumber;
    uint16_t    compression;
    uint16_t    range;
    uint16_t    encoder1LSB;
    uint16_t    encoder1MSB;
    uint16_t    encoder2LSB;
    uint16_t    encoder2MSB;
    uint16_t    accelerometerX;
    uint16_t    accelerometerY;
    uint16_t    accelerometerZ;
    uint16_t    giroscopeX;
    uint16_t    giroscopeY;
    uint16_t    giroscopeZ;
    uint16_t    compassX;
    uint16_t    compassY;
    uint16_t    compassZ;
} MiniUtScanHeader;

typedef struct
{
    uint16_t    gate1Amplitude;
    uint16_t    gate1Info;
    uint16_t    gate2Amplitude;
    uint16_t    gate2Info;
    uint16_t    gate3Amplitude;
    uint16_t    gate3Info;
    uint16_t    gate4Amplitude;
    uint16_t    gate4Info;
} MiniUtScanFooter;



// Firmware Version Type
#define FIRMWARE_VERSION_USB	0x00000000
#define FIRMWARE_VERSION_BT		0x40000000
#define FIRMWARE_VERSION_WIFI	0x80000000
#define FIRMWARE_VERSION_EHTER	0xC0000000

}
}
}  // ungava::cartridge::mini_ut

#endif // MINI_UT_DEFINITION_H
