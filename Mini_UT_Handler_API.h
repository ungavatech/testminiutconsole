#ifndef MINI_UT_HANDLER_API_H
#define MINI_UT_HANDLER_API_H

#include "Acquisition_Handler_Definitions.h"
#include "Mini_UT_Definitions.h"
#include "Communication.h"


using namespace ungava;
using namespace ungava::cartridge;
using namespace ungava::cartridge::mini_ut;

extern "C"
{
    #ifdef MINI_UT_HANDLERDLL_EXPORT
        #define MINI_UT_HANDLERDLL_API __declspec(dllexport)
    #else
        #define MINI_UT_HANDLERDLL_API __declspec(dllimport)
    #endif

    MINI_UT_HANDLERDLL_API void     getDllVersion(char * version);
    MINI_UT_HANDLERDLL_API int      initialize(const CommunicationType type, char * arg);
    MINI_UT_HANDLERDLL_API int      getNumberOfDevices();
    MINI_UT_HANDLERDLL_API int      getFirmwareVersion(const int index, uint32_t * version);
    //Configuration registers
    MINI_UT_HANDLERDLL_API int      getFpgaVersion(const int index, uint32_t * version);
    MINI_UT_HANDLERDLL_API int      getDigitalBoardSerialNumber(const int index, uint32_t * serialNumber);
    MINI_UT_HANDLERDLL_API int      setDigitalBoardSerialNumber(const int index, const uint32_t serialNumber);
    MINI_UT_HANDLERDLL_API int      getAnalogBoardSerialNumber(const int index, uint32_t * serialNumber);
    MINI_UT_HANDLERDLL_API int      setAnalogBoardSerialNumber(const int index, const uint32_t serialNumber);
    MINI_UT_HANDLERDLL_API bool     isMux8Present(const int index);

    MINI_UT_HANDLERDLL_API int      setManualReset(const int index, const onOffState manualReset);
    MINI_UT_HANDLERDLL_API int      setResetDma(const int index, const onOffState resetDma);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTest(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTestMode(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTestStart(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTestProgress(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTestDone(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getZbtSelfTestZbt1(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      setZbtSelfTest(const int index, const onOffState zbtMode, const onOffState start);

    MINI_UT_HANDLERDLL_API int      getAdcSelfTest(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getAdcSelfTestRamp(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getAdcSelfTestData(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      getAdcSelfTestError(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int      setAdcSelfTest(const int index, const onOffState data, const onOffState ramp);

    MINI_UT_HANDLERDLL_API int      getDigitalBoardTemperature(const int index, int * temperature);
    MINI_UT_HANDLERDLL_API int      getAnalogBoardTemperature(const int index, int * temperature);

    MINI_UT_HANDLERDLL_API int      setHighVoltageEnable(const int index, const onOffState enable);
    MINI_UT_HANDLERDLL_API int      setHighVoltage(const int index, const uint32_t highVoltage, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setTxPulseWidth(const int index, const uint32_t halfPeriod, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setRxGainValue1(const int index, const float value, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setRxGainTime(const int index, const uint32_t timeIndex, const uint32_t time, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setRxGainSlope(const int index, const uint32_t slopeIndex, const float slope, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setRxGainTCGMaxValue(int index, const float value, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setRxGainTCGEnable(int index, const onOffState enable, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setAutomaticDCOffset(const int index, const automaticDcOffset offset);
    MINI_UT_HANDLERDLL_API int      setDaqTrigger(const int index, const onOffState trigger);

    MINI_UT_HANDLERDLL_API int      setNumberOfChannels(const int index, uint32_t numberOfChannels);

    MINI_UT_HANDLERDLL_API int      setDaqDelay(const int index, const uint32_t delay, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setDaqRange(const int index, uint32_t range, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setSamplingFrequency(const int index, const samplingFrequency frequency);
    MINI_UT_HANDLERDLL_API int      setCompressionFactor(const int index, const uint32_t factor, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setTriggerMode(const int index, const triggerMode mode);
    MINI_UT_HANDLERDLL_API int      setTriggerInternalFrequency(const int index, const uint32_t frequency, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setCollectionEnable(const int index, const onOffState enable);

    MINI_UT_HANDLERDLL_API int      getNumberOfAvailableTriggers(const int index, uint32_t * availTrigger);
    MINI_UT_HANDLERDLL_API int      setCollectNumberOfTriggers(const int index, const uint32_t number);

    MINI_UT_HANDLERDLL_API int      setHeaderFooterAndAScan(const int index, const uint32_t content, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setAveraging(const int index, const log2AverageValue average, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setAdcPrecision(const int index, const adcPrecision precision, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setPitchAndCatch(const int index, const onOffState value, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setMuxChainWord(const int index, const muxChainBit txChain, const muxChainBit rxChain,
                                                    const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setAnalogBandpassFilter(const int index, const bandpassFilterFreq frequency, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setFirFilterSelect(const int index, const firFilterFreq selection, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setGateStart(const int index, const gateNumber gate, const int32_t delay, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setGateEnd(const int index, const gateNumber gate, const int32_t duration, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setGateThreshold(const int index, const gateNumber gate, const int32_t threshold, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setGateControl(const int index, const gateNumber gate, const onOffState active,
                                                   const gateControlFollow follow, const gateControlType type,
                                                   const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      setGate1InterfaceTrigger(const int index, const onOffState state, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      setGate1InterfaceTriggerDelayAntecedence(const int index, const interfaceTriggerMode mode,
                                                                             const uint32_t time, const uint32_t channel);

    MINI_UT_HANDLERDLL_API int      getEncoderPosition(const int index, const encoderName encoder, int32_t * position);
    MINI_UT_HANDLERDLL_API int      setEncoderClear(const int index, const encoderName encoder);
    MINI_UT_HANDLERDLL_API int      setEncoderType(const int index, const encoderName encoder, const encoderType type);
    MINI_UT_HANDLERDLL_API int      setRectificationType(const int index, const rectificationType type, const uint32_t channel);
    MINI_UT_HANDLERDLL_API int      getEncoderTrigIncrement(const int index, const encoderName encoder, uint32_t * ticks);
    MINI_UT_HANDLERDLL_API int      setEncoderTrigIncrement(const int index, const encoderName encoder, uint32_t ticks);
    MINI_UT_HANDLERDLL_API int      getHeaderEncoderReporting(const int index, encoderReportingType * type);
    MINI_UT_HANDLERDLL_API int      setHeaderEncoderReporting(const int index, encoderReportingType type);

    MINI_UT_HANDLERDLL_API int      getAScanData(const int index, unsigned char* buf, const uint32_t bufSize, const uint32_t maxNbOfScan, uint32_t * nbScan);
    MINI_UT_HANDLERDLL_API int      setPrfTest(const int index, const onOffState state);

    MINI_UT_HANDLERDLL_API int      updateFirmware(const int index, unsigned char *buf, const uint32_t bufSize, const miniutFirmwareUpdateType type);
    MINI_UT_HANDLERDLL_API int      configureDeviceNetwork(const int index, char *ipAddr, char *netmask, char *gateway, IpConfigType type);
    MINI_UT_HANDLERDLL_API int      restoreFailsafeDeviceNetwork(const int index);
    MINI_UT_HANDLERDLL_API int      updateWirelessModuleFirmware(const int index, unsigned char *bug, const uint32_t bufSize);
    MINI_UT_HANDLERDLL_API int      getCommModuleType(const int index, commModuleType * type);


    // For test purposes only
    MINI_UT_HANDLERDLL_API int getLed(const int index, int32_t * led);
    MINI_UT_HANDLERDLL_API int setLed(const int index, const uint32_t led);
    MINI_UT_HANDLERDLL_API int getRampPattern(const int index, int32_t * pattern);
    MINI_UT_HANDLERDLL_API int setRampPattern(const int index, const uint32_t pattern);
    MINI_UT_HANDLERDLL_API int getRampIncrement(const int index, int32_t * increment);
    MINI_UT_HANDLERDLL_API int setRampIncrement(const int index, const uint32_t increment);
    MINI_UT_HANDLERDLL_API int readTestQuadSpiFlash(const int index, uint32_t * value);
    MINI_UT_HANDLERDLL_API int writeTestQuadSpiFlash(const int index, const uint32_t value);
    MINI_UT_HANDLERDLL_API int getQuadSpiID(const int index, uint32_t * id);
}

#endif  // MINI_UT_HANDLER_API_H
