#ifndef MINI_UT_STRUCT_H
#define MINI_UT_STRUCT_H

#include <inttypes.h>

namespace ungava { namespace cartridge { namespace mini_ut {


typedef struct {
    uint32_t fpgaVersion;
    uint32_t manualReset;
    uint32_t dmaReset;
    uint32_t zbtSelfTest;
    uint32_t adcSelfTestError;
    uint32_t highVoltage[8];
    uint32_t txPulseWidth[8];
    uint32_t rectificationType[8];
    uint32_t automaticDCOffset;
    uint32_t muxChainWord[8];
    uint32_t analogBandpassFilter[8];
    uint32_t firFilterSelect[8];
    uint32_t mux8Present;
    uint32_t highVoltageEnable;
    uint32_t pitchAndCatch[8];
    uint32_t commModulePresent;
    uint32_t rxGainTCGMaxValue[8];
    uint32_t rxGainTCGEnable[8];
    uint32_t rxGainValue1[8];
    uint32_t rxGainTime1[8];
    uint32_t rxGainSlope1[8];
    uint32_t rxGainTime2[8];
    uint32_t rxGainSlope2[8];
    uint32_t rxGainTime3[8];
    uint32_t rxGainSlope3[8];
    uint32_t rxGainTime4[8];
    uint32_t rxGainSlope4[8];
    uint32_t rxGainTime5[8];
    uint32_t rxGainSlope5[8];
    uint32_t rxGainTime6[8];
    uint32_t rxGainSlope6[8];
    uint32_t rxGainTime7[8];
    uint32_t rxGainSlope7[8];
    uint32_t rxGainTime8[8];
    uint32_t rxGainSlope8[8];
    uint32_t rxGainTime9[8];
    uint32_t rxGainSlope9[8];
    uint32_t rxGainTime10[8];
    uint32_t rxGainSlope10[8];
    uint32_t rxGainTime11[8];
    uint32_t rxGainSlope11[8];
    uint32_t rxGainTime12[8];
    uint32_t rxGainSlope12[8];
    uint32_t rxGainTime13[8];
    uint32_t rxGainSlope13[8];
    uint32_t rxGainTime14[8];
    uint32_t rxGainSlope14[8];
    uint32_t rxGainTime15[8];
    uint32_t rxGainSlope15[8];
    uint32_t rxGainTime16[8];
    uint32_t daqTrigger;
    uint32_t numberOfChannels;
    uint32_t daqDelay[8];
    uint32_t daqRange[8];
    uint32_t samplingFrequency;
    uint32_t compressionFactor[8];
    uint32_t triggerMode;
    uint32_t triggerInternalFrequency[8];
    uint32_t collectionEnable;
    uint32_t numberOfAvailableTriggers;
    uint32_t collectNumberOfTriggers;
    uint32_t headerFooterAndAScan[8];
    uint32_t blankPulsesDisable;
    uint32_t averaging[8];
    uint32_t adcPrecision[8];
    uint32_t gate1Start[8];
    uint32_t gate1End[8];
    uint32_t gate1Threshold[8];
    uint32_t gate1Control[8];
    uint32_t gate2Start[8];
    uint32_t gate2End[8];
    uint32_t gate2Threshold[8];
    uint32_t gate2Control[8];
    uint32_t gate3Start[8];
    uint32_t gate3End[8];
    uint32_t gate3Threshold[8];
    uint32_t gate3Control[8];
    int32_t  gate4Start[8];
    int32_t  gate4End[8];
    uint32_t gate4Threshold[8];
    uint32_t gate4Control[8];
    uint32_t gate1InterfaceTrigger[8];
    uint32_t gate1InterfaceTriggerDelayAntecedence[8];
    int32_t  encoder1Position;
    int32_t  encoder2Position;
    uint32_t encoder1Clear;
    uint32_t encoder2Clear;
    uint32_t encoder1Type;
    uint32_t encoder2Type;
    uint32_t encoder1TrigIncrement;
    uint32_t encoder2TrigIncrement;
    uint32_t headerEncoderReporting;
    uint32_t prfTest;
    uint32_t rampPattern;
    uint32_t rampIncrementDecrement;
    uint32_t nbOfSamplePerScan[8];
    uint32_t totalScanSize[8];
} MINI_UT_FpgaMap;

    
typedef struct {
    uint32_t digitalBoardSerialNumber;
    uint32_t analogBoardSerialNumber;
    int32_t  digitalBoardTemperature;
    int32_t  analogBoardTemperature;
    uint32_t ascanTransferSize;
    uint32_t led;
} MINI_UT_ARM;

}}}  //ungava::cartridge::mini_ut

#endif // MINI_UT_STRUCT_H
