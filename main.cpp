#include <iostream>
#include <conio.h>
#include <string.h>
#include "Acquisition_Handler_Definitions.h"
#include "Mini_UT_Definitions.h"
#include "Communication.h"
#include "Mini_UT_Handler_API.h"

using namespace std;

#define MAX_TRANSFER_SIZE (4 * 40 * 1024)

unsigned char buf[MAX_TRANSFER_SIZE];

int main(int argc, char *argv[])
{
    int32_t axis1, axis2;
    uint32_t realScanToRead = 0;
    uint32_t loopNumber = 0;
    uint32_t daqRange = 3000;
    uint32_t internalPRF = 5000;
    uint32_t encInc1 = 1;
    uint32_t encInc2 = 1;

    if (argc != 6)
    {
        printf("Usage: TestMiniUTConsole IP address (OR USB) RANGE PRF INC1 INC2\n");
        getch();
        return 0;
    }

    daqRange = atoi(argv[2]);
    internalPRF = atoi(argv[3]);
    encInc1 = atoi(argv[4]);
    encInc2 = atoi(argv[5]);

    printf("Range: %d\n", daqRange);
    printf("PRF:   %d\n", internalPRF);
    printf("Inc1:  %d\n", encInc1);
    printf("Inc2:  %d\n", encInc2);

    if (!strcmp(argv[1], "USB"))
    {
         initialize(USB_PORT, nullptr);
    }
    else
    {
        initialize(ETHERNET_PORT, argv[1]);
    }

    setRxGainValue1(0, 45, 0);
    setRxGainTCGMaxValue(0, 92, 0);

    setEncoderType(0, ENCODER_1, QUADRATUR);
    setEncoderType(0, ENCODER_2, QUADRATUR);
    setEncoderTrigIncrement(0, ENCODER_1, encInc1);
    setEncoderTrigIncrement(0, ENCODER_2, encInc2);

    setTriggerMode(0, ENCODER_MODE);
    setTriggerInternalFrequency(0, internalPRF, 0);
    setRectificationType(0, RAW_SIGNAL_BYPASS, 0);
    setTxPulseWidth(0, 100, 0);
    setDaqDelay(0, 0, 0);
    setDaqRange(0, daqRange, 0);

    setGateControl(0, GATE_1, ON, FROM_T0, MAXIMUM_PEAK, 0);
    setGateStart(0, GATE_1, 500, 0);
    setGateEnd(0, GATE_1, 600, 0);
    setGateThreshold(0, GATE_1, 50, 0);

    setHighVoltageEnable(0, ON);
    setAutomaticDCOffset(0, DISABLED_OFFSET);
    setCollectionEnable(0, ON);

    do
    {
        uint32_t nbScan;

        getNumberOfAvailableTriggers(0, &realScanToRead);

        getAScanData(0, buf, MAX_TRANSFER_SIZE, realScanToRead, &nbScan);

        getEncoderPosition(0, ENCODER_1, &axis1);
        getEncoderPosition(0, ENCODER_2, &axis2);
        printf("\rLoop: %08d Axis 1: %04d Axis 2: %04d FIFO: %04d Read: %04d         ", loopNumber++, axis1, axis2, realScanToRead, nbScan);
    } while (!kbhit());

    printf("\n");
    return 0;
}
